class Empleado: #clase --> plantilla que sirve para crear instacias
    """Un ejemplo de clase para Empleados"""
 def __int__(self, nomina, nombre):  #constructor (método, hace cosas conmigo, me construye)
    self.nomina = nomina #atributo ('yo tengo')
    self.nombre = nombre #atributo

 def CalculoImpuestos (self): #función hace cosas con datos --> pasa a ser un método
    impuestos = self.nomina*0.30 #variable ('yo no tengo') --> pasa a atributo (mi nómina multiplícala por 0,3)
    print ("El empleado {name} debe pagar {tax:.2f}".format (name= self.nombre, tax=impuestos))
    return impuestos

 def displayCost(total):
   print("Los impuestos a pagar en total son {:.2f} euros".format(total))

empleadoPepe = Empleado(30000, Pepe)  #instancia (ejemplo de una clase)
empleadaAna = Empleado(20000, Ana)    #instancia

total = empleadoPepe.CalculoImpuestos + empleadaAna.CalculoImpuestos

displayCost(empleadoPepe.CalculoImpuestos + empleadaAna.CalculoImpuestos)
